<?php
/**
 * ProjectListResponseV1Test
 *
 * PHP version 5
 *
 * @category Class
 * @package  PhotoRobotSDK
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * PhotoRobot Cloud REST API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.30
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace PhotoRobotSDK;

/**
 * ProjectListResponseV1Test Class Doc Comment
 *
 * @category    Class
 * @description ProjectListResponseV1
 * @package     PhotoRobotSDK
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class ProjectListResponseV1Test extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "ProjectListResponseV1"
     */
    public function testProjectListResponseV1()
    {
    }

    /**
     * Test attribute "offset"
     */
    public function testPropertyOffset()
    {
    }

    /**
     * Test attribute "limit"
     */
    public function testPropertyLimit()
    {
    }

    /**
     * Test attribute "sort"
     */
    public function testPropertySort()
    {
    }

    /**
     * Test attribute "data"
     */
    public function testPropertyData()
    {
    }
}
