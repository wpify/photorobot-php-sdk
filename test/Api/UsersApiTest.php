<?php
/**
 * UsersApiTest
 * PHP version 5
 *
 * @category Class
 * @package  PhotoRobotSDK
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * PhotoRobot Cloud REST API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.30
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the endpoint.
 */

namespace PhotoRobotSDK;

use PhotoRobotSDK\Configuration;
use PhotoRobotSDK\ApiException;
use PhotoRobotSDK\ObjectSerializer;

/**
 * UsersApiTest Class Doc Comment
 *
 * @category Class
 * @package  PhotoRobotSDK
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class UsersApiTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test cases
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test case for restV1ApiUserControllerGet
     *
     * Get user.
     *
     */
    public function testRestV1ApiUserControllerGet()
    {
    }

    /**
     * Test case for restV1ApiUserControllerList
     *
     * Get list of users.
     *
     */
    public function testRestV1ApiUserControllerList()
    {
    }
}
