<?php
/**
 * StopResponseV1
 *
 * PHP version 5
 *
 * @category Class
 * @package  PhotoRobotSDK
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * PhotoRobot Cloud REST API
 *
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: 1.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.30
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace PhotoRobotSDK\Model;

use \ArrayAccess;
use \PhotoRobotSDK\ObjectSerializer;

/**
 * StopResponseV1 Class Doc Comment
 *
 * @category Class
 * @package  PhotoRobotSDK
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class StopResponseV1 implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'StopResponseV1';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'id' => 'string',
'dir_name' => 'string',
'swing_abs' => 'float',
'turn_abs' => 'float',
'label' => 'string',
'gs1' => 'string',
'retouch' => 'bool',
'item_id' => 'string',
'original' => 'AllOfStopResponseV1Original',
'edited' => 'AllOfStopResponseV1Edited'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'id' => null,
'dir_name' => null,
'swing_abs' => null,
'turn_abs' => null,
'label' => null,
'gs1' => null,
'retouch' => null,
'item_id' => null,
'original' => null,
'edited' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'id' => 'id',
'dir_name' => 'dirName',
'swing_abs' => 'swingAbs',
'turn_abs' => 'turnAbs',
'label' => 'label',
'gs1' => 'gs1',
'retouch' => 'retouch',
'item_id' => 'itemId',
'original' => 'original',
'edited' => 'edited'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'id' => 'setId',
'dir_name' => 'setDirName',
'swing_abs' => 'setSwingAbs',
'turn_abs' => 'setTurnAbs',
'label' => 'setLabel',
'gs1' => 'setGs1',
'retouch' => 'setRetouch',
'item_id' => 'setItemId',
'original' => 'setOriginal',
'edited' => 'setEdited'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'id' => 'getId',
'dir_name' => 'getDirName',
'swing_abs' => 'getSwingAbs',
'turn_abs' => 'getTurnAbs',
'label' => 'getLabel',
'gs1' => 'getGs1',
'retouch' => 'getRetouch',
'item_id' => 'getItemId',
'original' => 'getOriginal',
'edited' => 'getEdited'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['id'] = isset($data['id']) ? $data['id'] : null;
        $this->container['dir_name'] = isset($data['dir_name']) ? $data['dir_name'] : null;
        $this->container['swing_abs'] = isset($data['swing_abs']) ? $data['swing_abs'] : null;
        $this->container['turn_abs'] = isset($data['turn_abs']) ? $data['turn_abs'] : null;
        $this->container['label'] = isset($data['label']) ? $data['label'] : null;
        $this->container['gs1'] = isset($data['gs1']) ? $data['gs1'] : null;
        $this->container['retouch'] = isset($data['retouch']) ? $data['retouch'] : null;
        $this->container['item_id'] = isset($data['item_id']) ? $data['item_id'] : null;
        $this->container['original'] = isset($data['original']) ? $data['original'] : null;
        $this->container['edited'] = isset($data['edited']) ? $data['edited'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['dir_name'] === null) {
            $invalidProperties[] = "'dir_name' can't be null";
        }
        if ($this->container['swing_abs'] === null) {
            $invalidProperties[] = "'swing_abs' can't be null";
        }
        if ($this->container['turn_abs'] === null) {
            $invalidProperties[] = "'turn_abs' can't be null";
        }
        if ($this->container['label'] === null) {
            $invalidProperties[] = "'label' can't be null";
        }
        if ($this->container['gs1'] === null) {
            $invalidProperties[] = "'gs1' can't be null";
        }
        if ($this->container['retouch'] === null) {
            $invalidProperties[] = "'retouch' can't be null";
        }
        if ($this->container['item_id'] === null) {
            $invalidProperties[] = "'item_id' can't be null";
        }
        if ($this->container['original'] === null) {
            $invalidProperties[] = "'original' can't be null";
        }
        if ($this->container['edited'] === null) {
            $invalidProperties[] = "'edited' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets id
     *
     * @return string
     */
    public function getId()
    {
        return $this->container['id'];
    }

    /**
     * Sets id
     *
     * @param string $id Unique ID of the stop.
     *
     * @return $this
     */
    public function setId($id)
    {
        $this->container['id'] = $id;

        return $this;
    }

    /**
     * Gets dir_name
     *
     * @return string
     */
    public function getDirName()
    {
        return $this->container['dir_name'];
    }

    /**
     * Sets dir_name
     *
     * @param string $dir_name Name of the folder in which this stop is.
     *
     * @return $this
     */
    public function setDirName($dir_name)
    {
        $this->container['dir_name'] = $dir_name;

        return $this;
    }

    /**
     * Gets swing_abs
     *
     * @return float
     */
    public function getSwingAbs()
    {
        return $this->container['swing_abs'];
    }

    /**
     * Sets swing_abs
     *
     * @param float $swing_abs Vertical angle at which this stop has been captured (also known as swing angle).
     *
     * @return $this
     */
    public function setSwingAbs($swing_abs)
    {
        $this->container['swing_abs'] = $swing_abs;

        return $this;
    }

    /**
     * Gets turn_abs
     *
     * @return float
     */
    public function getTurnAbs()
    {
        return $this->container['turn_abs'];
    }

    /**
     * Sets turn_abs
     *
     * @param float $turn_abs Horizontal angle at which this stop has been captured.
     *
     * @return $this
     */
    public function setTurnAbs($turn_abs)
    {
        $this->container['turn_abs'] = $turn_abs;

        return $this;
    }

    /**
     * Gets label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->container['label'];
    }

    /**
     * Sets label
     *
     * @param string $label Label associated with the stop.
     *
     * @return $this
     */
    public function setLabel($label)
    {
        $this->container['label'] = $label;

        return $this;
    }

    /**
     * Gets gs1
     *
     * @return string
     */
    public function getGs1()
    {
        return $this->container['gs1'];
    }

    /**
     * Sets gs1
     *
     * @param string $gs1 GS1 label associated with the stop.
     *
     * @return $this
     */
    public function setGs1($gs1)
    {
        $this->container['gs1'] = $gs1;

        return $this;
    }

    /**
     * Gets retouch
     *
     * @return bool
     */
    public function getRetouch()
    {
        return $this->container['retouch'];
    }

    /**
     * Sets retouch
     *
     * @param bool $retouch Marked for retouch by external retoucher.
     *
     * @return $this
     */
    public function setRetouch($retouch)
    {
        $this->container['retouch'] = $retouch;

        return $this;
    }

    /**
     * Gets item_id
     *
     * @return string
     */
    public function getItemId()
    {
        return $this->container['item_id'];
    }

    /**
     * Sets item_id
     *
     * @param string $item_id ID of the associated item.
     *
     * @return $this
     */
    public function setItemId($item_id)
    {
        $this->container['item_id'] = $item_id;

        return $this;
    }

    /**
     * Gets original
     *
     * @return AllOfStopResponseV1Original
     */
    public function getOriginal()
    {
        return $this->container['original'];
    }

    /**
     * Sets original
     *
     * @param AllOfStopResponseV1Original $original Original captured image.
     *
     * @return $this
     */
    public function setOriginal($original)
    {
        $this->container['original'] = $original;

        return $this;
    }

    /**
     * Gets edited
     *
     * @return AllOfStopResponseV1Edited
     */
    public function getEdited()
    {
        return $this->container['edited'];
    }

    /**
     * Sets edited
     *
     * @param AllOfStopResponseV1Edited $edited Image after edit.
     *
     * @return $this
     */
    public function setEdited($edited)
    {
        $this->container['edited'] = $edited;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
