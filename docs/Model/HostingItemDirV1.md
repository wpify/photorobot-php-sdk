# HostingItemDirV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** |  | 
**type** | **string** |  | 
**spin** | [**\PhotoRobotSDK\Model\HostingItemDirSpinV1**](HostingItemDirSpinV1.md) |  | [optional] 
**images** | [**\PhotoRobotSDK\Model\HostingItemDirImageV1[]**](HostingItemDirImageV1.md) |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

