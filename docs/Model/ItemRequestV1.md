# ItemRequestV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Unique ID of the item. | 
**name** | **string** | Unique name of the item within the project. | 
**project_id** | **string** | The id of the project to which the item belongs. | 
**workspace_id** | **string** | Id of the workspace on which the item is photographed. | 
**preset_id** | **string** | The preset id based on which the item is photographed. | 
**capture_status** | **string** | Status of the item. | 
**note** | **string** | Item notes. | 
**tags** | **string[]** | String array of the tags. | 
**barcode** | **string** | Barcode of the item. | 
**sku** | **string** | Tracking code of the item. | 
**tracking_code** | **string** | This property is deprecated, use sku instead. | 
**link** | **string** | External url of the item. | 
**dimensions** | [**\PhotoRobotSDK\Model\ItemDimensionV1[]**](ItemDimensionV1.md) | Item dimensions. | 
**dirs** | [**\PhotoRobotSDK\Model\ItemDirV1[]**](ItemDirV1.md) | Folders on the item. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

