# SaveStopsRequestBodyV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**data** | [**\PhotoRobotSDK\Model\StopResponseV1[]**](StopResponseV1.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

