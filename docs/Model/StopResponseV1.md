# StopResponseV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Unique ID of the stop. | [optional] 
**dir_name** | **string** | Name of the folder in which this stop is. | 
**swing_abs** | **float** | Vertical angle at which this stop has been captured (also known as swing angle). | 
**turn_abs** | **float** | Horizontal angle at which this stop has been captured. | 
**label** | **string** | Label associated with the stop. | 
**gs1** | **string** | GS1 label associated with the stop. | 
**retouch** | **bool** | Marked for retouch by external retoucher. | 
**item_id** | **string** | ID of the associated item. | 
**original** | [**AllOfStopResponseV1Original**](AllOfStopResponseV1Original.md) | Original captured image. | 
**edited** | [**AllOfStopResponseV1Edited**](AllOfStopResponseV1Edited.md) | Image after edit. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

