# HostingItemDirImageV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | 
**md5** | **string** |  | 
**order** | **float** |  | 
**url** | **string** |  | 
**swing_abs** | **float** |  | [optional] 
**turn_abs** | **float** |  | [optional] 
**label** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

