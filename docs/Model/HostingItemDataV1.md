# HostingItemDataV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** |  | 
**name** | **string** |  | 
**status** | **string** |  | 
**barcode** | **string** |  | [optional] 
**tracking_code** | **string** |  | [optional] 
**date_created** | **float** |  | 
**date_modified** | **float** |  | 
**dirs** | [**\PhotoRobotSDK\Model\HostingItemDirV1[]**](HostingItemDirV1.md) |  | 
**tags** | **string[]** | String array of the tags. | 
**deleted** | **bool** | True if the item was deleted otherwise not mentioned. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

