# PresetListResponseV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**offset** | **float** | Data offset that has been requested. | 
**limit** | **float** | Data limit that has been requested. | 
**sort** | **string** | Requested sort. | [default to 'noSort']
**data** | [**\PhotoRobotSDK\Model\PresetResponseV1[]**](PresetResponseV1.md) |  | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

