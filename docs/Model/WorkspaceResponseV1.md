# WorkspaceResponseV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Unique id of the workspace. | 
**name** | **string** | Name of the workspace. | 
**desc** | **string** | Description of the workspace. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

