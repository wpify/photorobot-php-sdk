# StatItemStatusChangeResponseV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**changed** | **float** | Timestamp change date. | 
**status** | **string** | Changed to status. | 
**item** | [**AllOfStatItemStatusChangeResponseV1Item**](AllOfStatItemStatusChangeResponseV1Item.md) | Items data | 
**user** | [**AllOfStatItemStatusChangeResponseV1User**](AllOfStatItemStatusChangeResponseV1User.md) | User data | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

