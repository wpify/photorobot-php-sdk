# ItemDirV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **string** | Folder name. | 
**type** | **string** | Folder type. | 
**viewer_url** | **string** | Url to PhotoRobot Viewer. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

