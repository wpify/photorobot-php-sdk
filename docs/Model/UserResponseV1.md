# UserResponseV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Unique id of the user. | 
**first_name** | **string** | First name of the user. | 
**last_name** | **string** | Last name of the user. | 
**email** | **string** | Email of the user. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

