# ItemDimensionV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**width** | **float** | Item width. | 
**height** | **float** | Item height. | 
**depth** | **float** | Item depth. | 
**weight** | **float** | Item weight. | 
**dir_name** | **string** | Name of the folder associated with this dimension entry. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

