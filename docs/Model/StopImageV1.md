# StopImageV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **string** | Hosting URL of the image. | 
**created** | **float** | When was this image captured. | 
**uploaded** | **float** | When was this image uploaded to the cloud. | 
**content_type** | **string** | Content type of the image. | 
**width** | **float** | Image width. | 
**height** | **float** | Image height. | 
**size** | **float** | Size of the image file (in bytes). | 
**retouched** | **bool** | If true, image has been processed externally. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

