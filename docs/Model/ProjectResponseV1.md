# ProjectResponseV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Unique ID of the project. | 
**name** | **string** | Name of the project. | 
**note** | **string** | Project notes. | 
**tags** | **string[]** | String array of the tags. | 
**created** | **float** | Timestamp of the created date. | 
**modified** | **float** | Timestamp of the modified date. | 
**created_by** | **string** | UserId who created project. | 
**modified_by** | **string** | UserId who modified project. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

