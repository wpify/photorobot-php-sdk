# PresetResponseV1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **string** | Unique id of the user. | 
**name** | **string** | Name of the preset. | 
**note** | **string** | Preset notes. | 
**tags** | **string[]** | String array of the tags. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)

