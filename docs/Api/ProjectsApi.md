# PhotoRobotSDK\ProjectsApi

All URIs are relative to *https://cloud2.photorobot.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**restV1ApiProjectControllerCreate**](ProjectsApi.md#restv1apiprojectcontrollercreate) | **POST** /api/rest/v1/projects | Create new project
[**restV1ApiProjectControllerDelete**](ProjectsApi.md#restv1apiprojectcontrollerdelete) | **DELETE** /api/rest/v1/projects/{id} | Delete project
[**restV1ApiProjectControllerGet**](ProjectsApi.md#restv1apiprojectcontrollerget) | **GET** /api/rest/v1/projects/{id} | Get project
[**restV1ApiProjectControllerList**](ProjectsApi.md#restv1apiprojectcontrollerlist) | **GET** /api/rest/v1/projects/list | Get list of projects
[**restV1ApiProjectControllerUpdate**](ProjectsApi.md#restv1apiprojectcontrollerupdate) | **PUT** /api/rest/v1/projects/{id} | Update project

# **restV1ApiProjectControllerCreate**
> restV1ApiProjectControllerCreate($body)

Create new project

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\ProjectsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \PhotoRobotSDK\Model\ProjectRequestV1(); // \PhotoRobotSDK\Model\ProjectRequestV1 | 

try {
    $apiInstance->restV1ApiProjectControllerCreate($body);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsApi->restV1ApiProjectControllerCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PhotoRobotSDK\Model\ProjectRequestV1**](../Model/ProjectRequestV1.md)|  |

### Return type

void (empty response body)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restV1ApiProjectControllerDelete**
> restV1ApiProjectControllerDelete($id)

Delete project

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\ProjectsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | 

try {
    $apiInstance->restV1ApiProjectControllerDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsApi->restV1ApiProjectControllerDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restV1ApiProjectControllerGet**
> \PhotoRobotSDK\Model\ProjectResponseV1 restV1ApiProjectControllerGet($id)

Get project

Get project with specified {id}.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\ProjectsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | 

try {
    $result = $apiInstance->restV1ApiProjectControllerGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsApi->restV1ApiProjectControllerGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\PhotoRobotSDK\Model\ProjectResponseV1**](../Model/ProjectResponseV1.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restV1ApiProjectControllerList**
> \PhotoRobotSDK\Model\ProjectListResponseV1 restV1ApiProjectControllerList($offset, $limit, $sort)

Get list of projects

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\ProjectsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$offset = 0; // float | How many rows to skip before returning result.
$limit = 1000; // float | Maximum number of rows in the result.
$sort = "noSort"; // string | How to sort the data.

try {
    $result = $apiInstance->restV1ApiProjectControllerList($offset, $limit, $sort);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsApi->restV1ApiProjectControllerList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **float**| How many rows to skip before returning result. | [optional] [default to 0]
 **limit** | **float**| Maximum number of rows in the result. | [optional] [default to 1000]
 **sort** | **string**| How to sort the data. | [optional] [default to noSort]

### Return type

[**\PhotoRobotSDK\Model\ProjectListResponseV1**](../Model/ProjectListResponseV1.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restV1ApiProjectControllerUpdate**
> restV1ApiProjectControllerUpdate($body, $id)

Update project

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\ProjectsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \PhotoRobotSDK\Model\ProjectRequestV1(); // \PhotoRobotSDK\Model\ProjectRequestV1 | 
$id = "id_example"; // string | 

try {
    $apiInstance->restV1ApiProjectControllerUpdate($body, $id);
} catch (Exception $e) {
    echo 'Exception when calling ProjectsApi->restV1ApiProjectControllerUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PhotoRobotSDK\Model\ProjectRequestV1**](../Model/ProjectRequestV1.md)|  |
 **id** | **string**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

