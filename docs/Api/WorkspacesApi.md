# PhotoRobotSDK\WorkspacesApi

All URIs are relative to *https://cloud2.photorobot.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**restV1ApiWorkspaceControllerGet**](WorkspacesApi.md#restv1apiworkspacecontrollerget) | **GET** /api/rest/v1/workspaces/{id} | Get workspace
[**restV1ApiWorkspaceControllerList**](WorkspacesApi.md#restv1apiworkspacecontrollerlist) | **GET** /api/rest/v1/workspaces/list | Get list of workspaces

# **restV1ApiWorkspaceControllerGet**
> \PhotoRobotSDK\Model\WorkspaceResponseV1 restV1ApiWorkspaceControllerGet($id)

Get workspace

Get workspace with specified {id}.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\WorkspacesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | 

try {
    $result = $apiInstance->restV1ApiWorkspaceControllerGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WorkspacesApi->restV1ApiWorkspaceControllerGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\PhotoRobotSDK\Model\WorkspaceResponseV1**](../Model/WorkspaceResponseV1.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restV1ApiWorkspaceControllerList**
> \PhotoRobotSDK\Model\WorkspaceListResponseV1 restV1ApiWorkspaceControllerList()

Get list of workspaces

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\WorkspacesApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->restV1ApiWorkspaceControllerList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling WorkspacesApi->restV1ApiWorkspaceControllerList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\PhotoRobotSDK\Model\WorkspaceListResponseV1**](../Model/WorkspaceListResponseV1.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

