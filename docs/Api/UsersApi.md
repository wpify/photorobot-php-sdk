# PhotoRobotSDK\UsersApi

All URIs are relative to *https://cloud2.photorobot.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**restV1ApiUserControllerGet**](UsersApi.md#restv1apiusercontrollerget) | **GET** /api/rest/v1/users/{id} | Get user
[**restV1ApiUserControllerList**](UsersApi.md#restv1apiusercontrollerlist) | **GET** /api/rest/v1/users/list | Get list of users

# **restV1ApiUserControllerGet**
> \PhotoRobotSDK\Model\UserResponseV1 restV1ApiUserControllerGet($id)

Get user

Get user with specified {id}.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | 

try {
    $result = $apiInstance->restV1ApiUserControllerGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->restV1ApiUserControllerGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\PhotoRobotSDK\Model\UserResponseV1**](../Model/UserResponseV1.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restV1ApiUserControllerList**
> \PhotoRobotSDK\Model\UserListResponseV1 restV1ApiUserControllerList()

Get list of users

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\UsersApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);

try {
    $result = $apiInstance->restV1ApiUserControllerList();
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling UsersApi->restV1ApiUserControllerList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**\PhotoRobotSDK\Model\UserListResponseV1**](../Model/UserListResponseV1.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

