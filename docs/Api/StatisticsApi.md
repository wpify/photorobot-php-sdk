# PhotoRobotSDK\StatisticsApi

All URIs are relative to *https://cloud2.photorobot.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**restV1ApiStatsControllerGet**](StatisticsApi.md#restv1apistatscontrollerget) | **GET** /api/rest/v1/stats/status-change | Get a change of status on items from a given day

# **restV1ApiStatsControllerGet**
> \PhotoRobotSDK\Model\StatItemStatusChangeResponseV1 restV1ApiStatsControllerGet($date, $project_id)

Get a change of status on items from a given day

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\StatisticsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$date = "date_example"; // string | The date of the day from which you want to get changes. Format YYYY-MM-DD
$project_id = "project_id_example"; // string | The project ID from which you want to get changes.

try {
    $result = $apiInstance->restV1ApiStatsControllerGet($date, $project_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling StatisticsApi->restV1ApiStatsControllerGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **date** | **string**| The date of the day from which you want to get changes. Format YYYY-MM-DD |
 **project_id** | **string**| The project ID from which you want to get changes. | [optional]

### Return type

[**\PhotoRobotSDK\Model\StatItemStatusChangeResponseV1**](../Model/StatItemStatusChangeResponseV1.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

