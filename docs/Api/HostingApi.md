# PhotoRobotSDK\HostingApi

All URIs are relative to *https://cloud2.photorobot.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**restV1ApiHostingControllerList**](HostingApi.md#restv1apihostingcontrollerlist) | **GET** /api/rest/v1/hosting/items | Returns a list of items and their links to spins and stills.

# **restV1ApiHostingControllerList**
> \PhotoRobotSDK\Model\HostingItemDataV1 restV1ApiHostingControllerList($project_id, $limit, $version, $include_all_images)

Returns a list of items and their links to spins and stills.

Returned image URLs accept additional parameters to set output type and size:  - **fm**: format of the image, jpg or webp (default jpg) - **q**: image quality, 1 - 100 (default 80) - **w**: desired width of the image - **h**: desired height of the image  Example URL: [https://hosting.photorobot.com/images/-ML2QkR2lrhwn5SVMaEu/-MQN8lV3J4rJubSG0mhc/FINAL/8f6JBKRk0fG6xThOsFsonw?fm=webp&w=1200&q=20](https://hosting.photorobot.com/images/-ML2QkR2lrhwn5SVMaEu/-MQN8lV3J4rJubSG0mhc/FINAL/8f6JBKRk0fG6xThOsFsonw?fm=webp&w=1200&q=20)

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\HostingApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$project_id = "project_id_example"; // string | The ID of the project.
$limit = 5000; // float | The upper limit on the number of records received during a single API call.
$version = 0; // float | Version of the data received in the last API call. Only items newer than specified version will be returned.
$include_all_images = true; // bool | Include links to all images for spins and animations.

try {
    $result = $apiInstance->restV1ApiHostingControllerList($project_id, $limit, $version, $include_all_images);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling HostingApi->restV1ApiHostingControllerList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **project_id** | **string**| The ID of the project. |
 **limit** | **float**| The upper limit on the number of records received during a single API call. | [default to 5000]
 **version** | **float**| Version of the data received in the last API call. Only items newer than specified version will be returned. | [optional] [default to 0]
 **include_all_images** | **bool**| Include links to all images for spins and animations. | [optional]

### Return type

[**\PhotoRobotSDK\Model\HostingItemDataV1**](../Model/HostingItemDataV1.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

