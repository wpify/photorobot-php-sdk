# PhotoRobotSDK\PresetsApi

All URIs are relative to *https://cloud2.photorobot.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**restV1ApiPresetControllerGet**](PresetsApi.md#restv1apipresetcontrollerget) | **GET** /api/rest/v1/presets/{id} | Get preset
[**restV1ApiPresetControllerList**](PresetsApi.md#restv1apipresetcontrollerlist) | **GET** /api/rest/v1/presets/list | Get list of presets

# **restV1ApiPresetControllerGet**
> \PhotoRobotSDK\Model\PresetResponseV1 restV1ApiPresetControllerGet($id)

Get preset

Get preset with specified {id}.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\PresetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | 

try {
    $result = $apiInstance->restV1ApiPresetControllerGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PresetsApi->restV1ApiPresetControllerGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\PhotoRobotSDK\Model\PresetResponseV1**](../Model/PresetResponseV1.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restV1ApiPresetControllerList**
> \PhotoRobotSDK\Model\PresetListResponseV1 restV1ApiPresetControllerList($offset, $limit, $sort)

Get list of presets

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\PresetsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$offset = 0; // float | How many rows to skip before returning result.
$limit = 1000; // float | Maximum number of rows in the result.
$sort = "noSort"; // string | How to sort the data.

try {
    $result = $apiInstance->restV1ApiPresetControllerList($offset, $limit, $sort);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling PresetsApi->restV1ApiPresetControllerList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **float**| How many rows to skip before returning result. | [optional] [default to 0]
 **limit** | **float**| Maximum number of rows in the result. | [optional] [default to 1000]
 **sort** | **string**| How to sort the data. | [optional] [default to noSort]

### Return type

[**\PhotoRobotSDK\Model\PresetListResponseV1**](../Model/PresetListResponseV1.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

