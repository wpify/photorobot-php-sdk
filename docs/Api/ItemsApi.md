# PhotoRobotSDK\ItemsApi

All URIs are relative to *https://cloud2.photorobot.com/*

Method | HTTP request | Description
------------- | ------------- | -------------
[**restV1ApiItemControllerCreate**](ItemsApi.md#restv1apiitemcontrollercreate) | **POST** /api/rest/v1/items | Create new item
[**restV1ApiItemControllerDelete**](ItemsApi.md#restv1apiitemcontrollerdelete) | **DELETE** /api/rest/v1/items/{id} | Delete item
[**restV1ApiItemControllerDeleteStops**](ItemsApi.md#restv1apiitemcontrollerdeletestops) | **DELETE** /api/rest/v1/items/{itemId}/stops | Delete item stops
[**restV1ApiItemControllerGet**](ItemsApi.md#restv1apiitemcontrollerget) | **GET** /api/rest/v1/items/{id} | Get item
[**restV1ApiItemControllerList**](ItemsApi.md#restv1apiitemcontrollerlist) | **GET** /api/rest/v1/items/list | Get list of items
[**restV1ApiItemControllerListStops**](ItemsApi.md#restv1apiitemcontrollerliststops) | **GET** /api/rest/v1/items/{itemId}/stops | Get list of stops for the specified item.
[**restV1ApiItemControllerSaveStops**](ItemsApi.md#restv1apiitemcontrollersavestops) | **POST** /api/rest/v1/items/{itemId}/stops | Create or update item stops
[**restV1ApiItemControllerUpdate**](ItemsApi.md#restv1apiitemcontrollerupdate) | **PUT** /api/rest/v1/items/{id} | Update item

# **restV1ApiItemControllerCreate**
> restV1ApiItemControllerCreate($body)

Create new item

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\ItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \PhotoRobotSDK\Model\ItemRequestV1(); // \PhotoRobotSDK\Model\ItemRequestV1 | 

try {
    $apiInstance->restV1ApiItemControllerCreate($body);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->restV1ApiItemControllerCreate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PhotoRobotSDK\Model\ItemRequestV1**](../Model/ItemRequestV1.md)|  |

### Return type

void (empty response body)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restV1ApiItemControllerDelete**
> restV1ApiItemControllerDelete($id)

Delete item

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\ItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | 

try {
    $apiInstance->restV1ApiItemControllerDelete($id);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->restV1ApiItemControllerDelete: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

void (empty response body)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restV1ApiItemControllerDeleteStops**
> restV1ApiItemControllerDeleteStops($body, $item_id)

Delete item stops

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\ItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \PhotoRobotSDK\Model\DeleteStopsRequestV1(); // \PhotoRobotSDK\Model\DeleteStopsRequestV1 | 
$item_id = "item_id_example"; // string | ID of the item.

try {
    $apiInstance->restV1ApiItemControllerDeleteStops($body, $item_id);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->restV1ApiItemControllerDeleteStops: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PhotoRobotSDK\Model\DeleteStopsRequestV1**](../Model/DeleteStopsRequestV1.md)|  |
 **item_id** | **string**| ID of the item. |

### Return type

void (empty response body)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restV1ApiItemControllerGet**
> \PhotoRobotSDK\Model\ItemResponseV1 restV1ApiItemControllerGet($id)

Get item

Get item with specified {id}.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\ItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$id = "id_example"; // string | 

try {
    $result = $apiInstance->restV1ApiItemControllerGet($id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->restV1ApiItemControllerGet: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **string**|  |

### Return type

[**\PhotoRobotSDK\Model\ItemResponseV1**](../Model/ItemResponseV1.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restV1ApiItemControllerList**
> \PhotoRobotSDK\Model\ItemListResponseV1 restV1ApiItemControllerList($offset, $limit, $sort, $project_id, $search, $sku, $barcode)

Get list of items

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\ItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$offset = 0; // float | How many rows to skip before returning result.
$limit = 1000; // float | Maximum number of rows in the result.
$sort = "noSort"; // string | How to sort the data.
$project_id = "project_id_example"; // string | ID of the project.
$search = "search_example"; // string | Search by name, sku or barcode.
$sku = "sku_example"; // string | Search by sku.
$barcode = "barcode_example"; // string | Search by barcode.

try {
    $result = $apiInstance->restV1ApiItemControllerList($offset, $limit, $sort, $project_id, $search, $sku, $barcode);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->restV1ApiItemControllerList: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **offset** | **float**| How many rows to skip before returning result. | [optional] [default to 0]
 **limit** | **float**| Maximum number of rows in the result. | [optional] [default to 1000]
 **sort** | **string**| How to sort the data. | [optional] [default to noSort]
 **project_id** | **string**| ID of the project. | [optional]
 **search** | **string**| Search by name, sku or barcode. | [optional]
 **sku** | **string**| Search by sku. | [optional]
 **barcode** | **string**| Search by barcode. | [optional]

### Return type

[**\PhotoRobotSDK\Model\ItemListResponseV1**](../Model/ItemListResponseV1.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restV1ApiItemControllerListStops**
> \PhotoRobotSDK\Model\WorkspaceListResponseV1 restV1ApiItemControllerListStops($item_id)

Get list of stops for the specified item.

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\ItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$item_id = "item_id_example"; // string | ID of the item.

try {
    $result = $apiInstance->restV1ApiItemControllerListStops($item_id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->restV1ApiItemControllerListStops: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **item_id** | **string**| ID of the item. |

### Return type

[**\PhotoRobotSDK\Model\WorkspaceListResponseV1**](../Model/WorkspaceListResponseV1.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restV1ApiItemControllerSaveStops**
> restV1ApiItemControllerSaveStops($body, $item_id)

Create or update item stops

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\ItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \PhotoRobotSDK\Model\SaveStopsRequestBodyV1(); // \PhotoRobotSDK\Model\SaveStopsRequestBodyV1 | 
$item_id = "item_id_example"; // string | ID of the item.

try {
    $apiInstance->restV1ApiItemControllerSaveStops($body, $item_id);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->restV1ApiItemControllerSaveStops: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PhotoRobotSDK\Model\SaveStopsRequestBodyV1**](../Model/SaveStopsRequestBodyV1.md)|  |
 **item_id** | **string**| ID of the item. |

### Return type

void (empty response body)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

# **restV1ApiItemControllerUpdate**
> \PhotoRobotSDK\Model\ItemResponseV1 restV1ApiItemControllerUpdate($body, $id)

Update item

### Example
```php
<?php
require_once(__DIR__ . '/vendor/autoload.php');
    // Configure HTTP bearer authorization: bearer
    $config = PhotoRobotSDK\Configuration::getDefaultConfiguration()
    ->setAccessToken('YOUR_ACCESS_TOKEN');


$apiInstance = new PhotoRobotSDK\Api\ItemsApi(
    // If you want use custom http client, pass your client which implements `GuzzleHttp\ClientInterface`.
    // This is optional, `GuzzleHttp\Client` will be used as default.
    new GuzzleHttp\Client(),
    $config
);
$body = new \PhotoRobotSDK\Model\ItemRequestV1(); // \PhotoRobotSDK\Model\ItemRequestV1 | 
$id = "id_example"; // string | 

try {
    $result = $apiInstance->restV1ApiItemControllerUpdate($body, $id);
    print_r($result);
} catch (Exception $e) {
    echo 'Exception when calling ItemsApi->restV1ApiItemControllerUpdate: ', $e->getMessage(), PHP_EOL;
}
?>
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | [**\PhotoRobotSDK\Model\ItemRequestV1**](../Model/ItemRequestV1.md)|  |
 **id** | **string**|  |

### Return type

[**\PhotoRobotSDK\Model\ItemResponseV1**](../Model/ItemResponseV1.md)

### Authorization

[bearer](../../README.md#bearer)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to Model list]](../../README.md#documentation-for-models) [[Back to README]](../../README.md)

